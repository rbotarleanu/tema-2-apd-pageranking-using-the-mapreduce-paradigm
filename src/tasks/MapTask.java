package tasks;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mapreduce.Utilities;

public class MapTask extends PartialSolution {

	// mapping result list
	public static List<MapResultHolder> results = new ArrayList<MapResultHolder>();

	private String document_name;
	private long offset;
	private long D;

	public MapTask() {
		super();
	}

	public MapTask(String document_name, long offset, long d) {
		super();
		this.document_name = document_name;
		this.offset = offset;
		D = d;
	}

	@Override
	public void processPartialSolution() throws IOException {
		MapTask mp = this;
		// the word histograms
		HashMap<Integer, Integer> myHashMap = new HashMap<Integer, Integer>();
		// the maximum length words
		ArrayList<String> myMaxLenWords = new ArrayList<String>();
		// the maximum length of a word
		int maximumWordLength = 0;

		// import REGEX and separators
		String regex = Utilities.REGEX;
		ArrayList<Integer> delims = Utilities.SEPARATORS;

		// open file
		RandomAccessFile raf = new RandomAccessFile(mp.getDocument_name(), "r");
		// look if we are at the beginning of a word
		raf.seek((mp.getOffset() - 1 > 0) ? (mp.getOffset() - 1) : mp
				.getOffset());
		long D = mp.getD() - 1;
		// check if we are in the middle of a word, if so skip
		while (mp.getOffset() != 0L) {
			int value = raf.read();
			if (delims.contains(value))
				break;
			--D;
		}
		if (D < 0) {
			// this fragment will have no more words to process, end
			raf.close();
			return;
		}
		// start splitting
		byte[] text = new byte[(int) D];
		raf.read(text);
		String txt = new String(text, "UTF-8");
		// check if the last word isn't finished
		if (!delims.contains(text[text.length - 1])) {
			// add to complete the last word in this fragment
			byte[] extra = new byte[1];
			int i = 0;
			while (true) {
				// read a character
				int b = raf.read();
				if (b == -1)
					break; // EOF
				if (delims.contains(b))
					break; // separator
				else
					extra[0] = (byte) b;
				// attach the character to the word
				txt += new String(extra, "UTF-8");
			}
		}
		// split in tokens
		String[] toks = txt.split(regex);
		for (String tok : toks) {
			// word length
			int len = tok.length();
			if (len == 0)
				continue;	// skip if token is empty
			if (myHashMap.containsKey(len))
				// increase word count for this length
				myHashMap.put(len, myHashMap.get(len) + 1);
			else
				// first word of this length
				myHashMap.put(len, 1);
			if (len > maximumWordLength) {
				// new local maximum
				maximumWordLength = len;
				myMaxLenWords.clear();
				myMaxLenWords.add(tok);
			} else if (len == maximumWordLength) {
				myMaxLenWords.add(tok); // add to maximum list
			}
		}
		synchronized (Utilities.GENERAL_LOCK) {
			// add to the mapping results
			results.add(new MapResultHolder(myHashMap, myMaxLenWords, mp
					.getDocument_name()));
		}
		raf.close();
	}

	@Override
	public String toString() {
		return "MAPPING for: " + document_name + "  from offset: " + offset
				+ "   of length" + D;
	}

	public String getDocument_name() {
		return document_name;
	}

	public void setDocument_name(String document_name) {
		this.document_name = document_name;
	}

	public Long getOffset() {
		return offset;
	}

	public void setOffset(Long offset) {
		this.offset = offset;
	}

	public Long getD() {
		return D;
	}

	public void setD(Long d) {
		D = d;
	}

}
