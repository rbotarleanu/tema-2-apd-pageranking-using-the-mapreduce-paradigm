package tasks;

import java.text.DecimalFormat;

public class ReduceResultHolder implements Comparable<ReduceResultHolder> {

	private int documentOrderInInput; // secondary condition for sorting
	private String docName;
	private double rank;
	private int maximumWordLength;
	private int maximumLengthWords;

	public ReduceResultHolder(String docName, double rank,
			int maximumWordLength, int maximumLengthWords) {
		super();
		this.docName = docName;
		this.rank = rank;
		this.maximumWordLength = maximumWordLength;
		this.maximumLengthWords = maximumLengthWords;
	}

	public String getDocName() {
		return docName;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}

	public double getRank() {
		return rank;
	}

	public void setRank(double rank) {
		this.rank = rank;
	}

	public int getMaximumWordLength() {
		return maximumWordLength;
	}

	public void setMaximumWordLength(int maximumWordLength) {
		this.maximumWordLength = maximumWordLength;
	}

	public int getMaximumLengthWords() {
		return maximumLengthWords;
	}

	public void setMaximumLengthWords(int maximumLengthWords) {
		this.maximumLengthWords = maximumLengthWords;
	}

	@Override
	public int compareTo(ReduceResultHolder other) {
		// truncate to 2 decimals
		float truncated_rank1 = (float) (Math.floor(rank * 100.0f) / 100.0f);
		float truncated_rank2 = (float) (Math.floor(other.rank * 100.0f) / 100.0f);
		if (truncated_rank1 < truncated_rank2)
			return 1;	// sort by rank first
		if (truncated_rank1 > truncated_rank2)
			return -1;
		// if rank is equal, consider the order in input
		return other.documentOrderInInput - documentOrderInInput;
	}

	@Override
	public String toString() {
		// print the ranking
		float truncated_rank = (float) (Math.floor(rank * 100.0f) / 100.0f);
		return docName + ";" + new DecimalFormat("0.00##").format(truncated_rank)
				+ ";[" + maximumWordLength + "," + maximumLengthWords + "]";
	}
}
