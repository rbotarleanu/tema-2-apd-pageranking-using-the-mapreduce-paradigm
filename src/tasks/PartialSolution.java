package tasks;

import java.io.IOException;

public abstract class PartialSolution {
	public abstract String toString();

	/**
	 * Procesarea unei solutii partiale. Aceasta poate implica generarea unor
	 * noi solutii partiale care se adauga in workpool folosind putWork(). Daca
	 * s-a ajuns la o solutie finala, aceasta va fi afisata.
	 * 
	 * @throws IOException
	 */
	public abstract void processPartialSolution() throws IOException;
}
