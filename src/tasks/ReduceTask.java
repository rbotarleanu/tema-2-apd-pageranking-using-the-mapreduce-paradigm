package tasks;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mapreduce.MapReduceParadigm;
import mapreduce.Utilities;

public class ReduceTask extends PartialSolution {

	/**
	 * The results of the Reduce workpool.
	 */
	public static List<ReduceResultHolder> documentsRanking = new ArrayList<ReduceResultHolder>();

	private List<String> maximumLengthWords = new ArrayList<String>();
	private HashMap<Integer, Integer> documentHistogram = new HashMap<Integer, Integer>();

	private String docName;
	private List<HashMap<Integer, Integer>> hash;
	private List<List<String>> maxLenWords;

	public ReduceTask(String docName, List<HashMap<Integer, Integer>> hash,
			List<List<String>> maxLenWords) {
		super();
		this.docName = docName;
		this.hash = hash;
		this.maxLenWords = maxLenWords;
	}

	public ReduceTask() {
		super();
	}

	@Override
	public void processPartialSolution() throws IOException {
		ReduceTask rw = this;
		combine();
		process();
	}

	/**
	 * Combines the result of the map phase for the document.
	 */
	private void combine() {
		int maximumWordLength = 0;
		// combine the local maximum length words into a global maximum(with
		// duplicates)
		for (List<String> maxLenWordList : maxLenWords) {
			if (maxLenWordList.get(0).length() > maximumWordLength) {
				maximumWordLength = maxLenWordList.get(0).length();
				maximumLengthWords.clear(); // discard current local maximum
				maximumLengthWords.addAll(maxLenWordList);
			} else if (maxLenWordList.get(0).length() == maximumWordLength) {
				maximumLengthWords.addAll(maxLenWordList);	// add to maximum
			}
		}
		// combine hashes
		for (HashMap<Integer, Integer> localHash : hash) {
			for (int i = 0; i <= maximumWordLength; ++i) {
				if (!localHash.containsKey(i))
					continue; // no value for this length
				int value = localHash.get(i);
				if (documentHistogram.containsKey(i))
					// add with previous value
					value += documentHistogram.get(i);
				documentHistogram.put(i, value);	// update the histogram
			}
		}
	}

	/**
	 * Computes the document rank.
	 */
	private void process() {
		// start computing the rank
		int maximumWordLength = maximumLengthWords.get(0).length();
		// compute rank
		// find the total number of words
		int totalWordCount = 0;
		for (int i = 0; i <= maximumWordLength; ++i) {
			if (!documentHistogram.containsKey(i))
				continue;
			totalWordCount += documentHistogram.get(i);
		}
			double rank = 0.0;
		for (int k = 0; k <= maximumWordLength; ++k) {
			if (!documentHistogram.containsKey(k))
				continue;
			rank += (Utilities.nFibo(k + 1) * documentHistogram.get(k))
					/ ((double) totalWordCount);
		}
		// find number of maximal distinct words
		int distinctMaximumLengthWords = 0;
		List<String> cache = new ArrayList<String>();
		for (String word : maximumLengthWords) {
			if (cache.contains(word))
				continue; // skip duplicates
			cache.add(word); // miss
			++distinctMaximumLengthWords;
		}
		// finished computation for this document, add to final results
		synchronized (Utilities.GENERAL_LOCK) { // thread safe
			documentsRanking.add(new ReduceResultHolder(docName, rank,
					maximumWordLength, distinctMaximumLengthWords));
		}
	}

	public String getDocName() {
		return docName;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}

	public List<HashMap<Integer, Integer>> getHash() {
		return hash;
	}

	public void setHash(List<HashMap<Integer, Integer>> hash) {
		this.hash = hash;
	}

	public List<List<String>> getMaxLenWords() {
		return maxLenWords;
	}

	public void setMaxLenWords(List<List<String>> maxLenWords) {
		this.maxLenWords = maxLenWords;
	}

	@Override
	public String toString() {
		return "ReduceWork [ " + "docName=" + docName + ", maxLenWords="
				+ maxLenWords + ",  hash=" + hash + "]";
	}

}
