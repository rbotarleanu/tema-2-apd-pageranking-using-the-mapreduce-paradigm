package tasks;

import java.util.HashMap;
import java.util.List;

/**
 * @author Botarleanu Robert-Mihai 331CB
 *	A holder class for the results of the mapping stage.
 *  Used for transporting data to the master thread.
 */
public class MapResultHolder {

	private HashMap<Integer, Integer> hash;	
	private List<String> maxLenWords;
	private String docName;
	
	public MapResultHolder(HashMap<Integer, Integer> hash,
			List<String> maxLenWords, String docName) {
		super();
		this.hash = hash;
		this.maxLenWords = maxLenWords;
		this.docName = docName;
	}
	
	public HashMap<Integer, Integer> getHash() {
		return hash;
	}
	
	public void setHash(HashMap<Integer, Integer> hash) {
		this.hash = hash;
	}
	
	public List<String> getMaxLenWords() {
		return maxLenWords;
	}
	
	public void setMaxLenWords(List<String> maxLenWords) {
		this.maxLenWords = maxLenWords;
	}
	
	public String getDocName() {
		return docName;
	}
	
	public void setDocName(String docName) {
		this.docName = docName;
	}
}	

