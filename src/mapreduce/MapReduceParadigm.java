package mapreduce;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import replicatedworkers.WorkPool;
import replicatedworkers.Worker;
import tasks.MapResultHolder;
import tasks.MapTask;
import tasks.ReduceResultHolder;
import tasks.ReduceTask;

/**
 * @author Botarleanu Robert-Mihai 331CB
 *	The MainClass and master Thread for the ReplicatedWorkers pattern.
 */
public class MapReduceParadigm {

	private static Integer NT;
	private 
	static String INPUT_FILE = "";
	private static String OUTPUT_FILE = "";
	private static String FILE_PREFIX = "";

	long D, ND;
	List<String> docs;
	List<Long> docs_size;

	boolean readInput() {
		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(new FileInputStream(
					new File(FILE_PREFIX + INPUT_FILE)), "UTF8"));
		} catch (UnsupportedEncodingException | FileNotFoundException e) {
			return false;
		}
		try {
			D = Long.parseLong(in.readLine());
			ND = Long.parseLong(in.readLine());
			docs = new ArrayList<String>();
			docs_size = new ArrayList<Long>();
			while (in.ready()) {
				String doc = in.readLine();
				docs.add(doc);
				File f = new File(FILE_PREFIX + doc);
				docs_size.add(f.length());
			}
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}

	public static void main(String[] args) throws UnsupportedEncodingException {
		// for scalability test
		long system_start_time = System.currentTimeMillis();
		
		MapReduceParadigm mapReduceParadigm = new MapReduceParadigm();

		try {
			NT = Integer.parseInt(args[0]);
			String[] toks = args[1].split("/");
			INPUT_FILE = toks[toks.length - 1];
			for (int i = 0; i < toks.length - 1; ++i)
				FILE_PREFIX += toks[i] + "/";
			OUTPUT_FILE = args[2];
		} catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
			System.err.println("Command line arguments invalid.");
			System.exit(1);
		}

		// read input from input file
		if (!mapReduceParadigm.readInput()) {
			System.err.println("Error reading file.");
			System.exit(2);
		}
		// Part 1 - Map
		WorkPool wp = new WorkPool(NT);
		// create the map tasks
		mapReduceParadigm.createMapTasks(wp);
		// prepare the workers
		List<Worker> workers = new ArrayList<Worker>();
		for (int i = 0; i < NT; ++i)
			workers.add(new Worker(wp));
		// start the computation
		for (int i = 0; i < NT; ++i)
			workers.get(i).start();
		for (int i = 0; i < NT; ++i)
			try {
				workers.get(i).join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		// PART2 - Reduce
		WorkPool wpReduce = new WorkPool(NT);
		// create the reduce tasks
		mapReduceParadigm.prepareReduceTasks(wpReduce);
		// prepare the workers
		List<Worker> reduce_workers = new ArrayList<Worker>();
		for (int i = 0; i < NT; ++i)
			reduce_workers.add(new Worker(wpReduce));
		// start the computation
		for (int i = 0; i < NT; ++i)
			reduce_workers.get(i).start();
		for (int i = 0; i < NT; ++i)
			try {
				reduce_workers.get(i).join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		try {
			mapReduceParadigm.printReduceCombineResults();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		long system_finish_time = System.currentTimeMillis();
		System.out.println((system_finish_time - system_start_time) / 1000.0);
	}

	private void printReduceCombineResults() throws FileNotFoundException, UnsupportedEncodingException {
		PrintWriter writer = new PrintWriter(OUTPUT_FILE, "UTF-8");
		Collections.sort(ReduceTask.documentsRanking);
		for(ReduceResultHolder reduceResultHolder: ReduceTask.documentsRanking)
			writer.println(reduceResultHolder);
		writer.close();
	}

	public void prepareReduceTasks(WorkPool wp) {
		HashMap<String, ReduceTask> reduceTasks = new HashMap<String, ReduceTask>();
		for (String doc : docs) {
			ReduceTask rt = new ReduceTask();
			rt.setHash(new ArrayList<HashMap<Integer,Integer>>());
			rt.setMaxLenWords(new ArrayList<List<String>>());
			rt.setDocName(FILE_PREFIX + doc); 
			reduceTasks.put(FILE_PREFIX + doc, rt);
			
		}
		for (MapResultHolder mrHolder : MapTask.results) {
			reduceTasks.get(mrHolder.getDocName()).getMaxLenWords()
					.add(mrHolder.getMaxLenWords());
			reduceTasks.get((mrHolder).getDocName()).getHash()
					.add(mrHolder.getHash());
		}
		for(String doc: docs) wp.putWork(reduceTasks.get(FILE_PREFIX + doc));
	}

	public void createMapTasks(WorkPool wp) {
		for (int i = 0; i < ND; ++i) {
			String doc_name = docs.get(i);
			long doc_size = docs_size.get(i);
			long offset = 0L;
			while (doc_size > D) {
				wp.putWork(new MapTask(FILE_PREFIX + doc_name, offset, D));
				offset += D;
				doc_size -= D;
			}
			wp.putWork(new MapTask(FILE_PREFIX + doc_name, offset, doc_size));
		}
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Number of documents: " + ND + ", Fragment size: "
				+ D);
		stringBuilder.append("\nDocuments are: {");
		for (int i = 0; i < docs.size(); ++i)
			stringBuilder.append("[" + docs.get(i) + "," + docs_size.get(i)
					+ "]");
		stringBuilder.append("}");
		return stringBuilder.toString();
	}
}
