package mapreduce;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class Utilities {
	public static String REGEX = "[;:/?~\\\\.,><~`\\[\\]{}()!@#$%^&\\-_+'=*\"|\\s+\\n\\r]";
	private static String separators = ";:/?~\\.,><~`[]{}()!@#$%^&-_+'=*\"| \t\n\r";
	private static byte[] seps;
	public static volatile ArrayList<Integer> SEPARATORS = null; // for contains

	// general use lock
	public static final Object GENERAL_LOCK = new Object();
	
	// n-Fibonnaci calculator
	public static int nFibo(int n) {
		// base case
		if( n == 0 ) return 0;
		if( n == 1 || n == 2 ) return 1;
		// start computing
		int t1 = 1, t2 = 1;
		// from n = 3 onwards
		n -= 2;
		while( n-- > 0 ) { 
			// t2 = t1 + t2, t1 = t2
			int aux = t1;
			t1 = t2;
			t2 += aux;
		}
		return t2; // the result
	}

	static {
		// prepare separator list
		try {
			seps = separators.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		SEPARATORS = new ArrayList<Integer>();
		for (byte b : seps)
			SEPARATORS.add((int) b);
	}

}
