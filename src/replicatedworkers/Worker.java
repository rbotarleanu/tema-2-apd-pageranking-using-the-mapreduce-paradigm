package replicatedworkers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import tasks.PartialSolution;
import tasks.ReduceTask;

// pornind de la scheletul laboratorului 5

public class Worker extends Thread {

	private WorkPool wp;

	public Worker(WorkPool workpool) {
		this.wp = workpool;
	}

	public void run() {
		while (true) {
			PartialSolution ps = wp.getWork();
			if (ps == null)
				break;
			try {
				ps.processPartialSolution();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}