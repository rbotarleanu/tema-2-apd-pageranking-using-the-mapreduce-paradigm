Botarleanu Robert-Mihai 331CB - Tema 2
--------------------------------------------------------------------------------
	Cuprins
		1. Descrierea problemei
		2. Descrierea ierarhiei programului
		3. Descrierea implementarii tiparului Replicated Workers
		4. Descrierea implementarii paradigmei MapReduce
			i) Etapa de mapare
			ii) Etapa de reducere
				- combinare
				- procesare
		5. Scalabilitate si consistenta
		6. Probleme intampinate si solutii gasite
		7. Compilare
--------------------------------------------------------------------------------

	1. 	Descrierea problemei
		Algoritmul implementat foloseste tiparul Replicated Workers in limbajul
	Java pentru a rezolva problema gasirii rangurilor unui set de documente si
	ordonarea lor in functie de acest criteriu.

	2.	Descrierea arhitecturii programului
		Programul este impartit in 3 pachete: 
			- mapreduce contine 2 clase: MapReduceParadigm(clasa principala a 
		programului si master thread-ul pattern-ului Replicated Workers.) si o
		clasa de utilitare Utilities, care contine variabile statice folosite in
		program, cat si o implementare iterativa a algoritmului n-Fibonacci.
			- replicatedworkers contine clasele Worker si WorkPool din cadrul 
		scheletului de cod al laboratorului 5, cu diferenta notabila ca functia 
		de procesare a unui task este acum prezenta in fiecare Task, worker-ul
		fiind o entitate generica ce va rula doar procesarea pentru task-ul
		primit(eliminand astfel specializarea sa si micsorand numarul de clase).
			- tasks contine clasa PartialSolution, din cadrul laboratorului 5,
		ce a fost convertita intr-o clasa abstracta folosita drept template 
		pentru clasele ReduceTask si MapTask, functiile mostenite fiind toString
		si processPartialSolution(functie ce va fi apelata de fiecare worker 
		pentru task-urile pe care le preia de la workpool). De asemenea, exista
		doua clase auxiliare MapResultHolder si ProduceResultHolder ce nu au alt
		rol decat a retine rezultatele date de fiecare etapa a paradigmei 
		MapReduce.
	
	3. 	Descrierea implementarii tiparului Replicated Workers
		Pattern-ul Replicated Workers a fost implementat pe baza scheletului 
	laboratorului 5, fiind folosita o clasa WorkPool ce are rolul de a primi
	si imparti task-uri ("work") pentru workeri.
		Clasa Worker reprezinta un worker generic, ce va primi un task din 
	lantul de mostenire a clasei PartialSolution si va rula 
	processPartialSolution pentru a determina rezultatul dorit(fie pentru 
	Map, fie pentru Reduce).
		Cele doua clase: MapTask si ReduceTask implementeaza efectiv algoritmul
	de MapReduce folosind modelul Replicated Workers.
	
	4. Descrierea implementarii paradigmei MapReduce
			i) Etapa de mapare
				Etapa de mapare, implementata in cadrul clasei MapTask va primi,
			conform enuntului, numele unui fisier, un offset de la inceputul 
			fisierului si o dimensiune a fragmentului pe care il va analiza.
				Fisierul se va deschide la offset-ul dat - 1, pentru a verifica
			daca inainte de inceperea fragmentului se afla un separator, sau,
			daca este nevoie sa se sara peste primele litere intalnite in 
			fragment.
				Ulterior, se vor citi din fisier numarul de bytes ramasi dupa 
			eliminarea primilor(daca este cazul). Se va forma un String ce va 
			fi tokenizat in cuvinte, iar apoi pentru fiecare cuvant i se va 
			determina lungimea pentru a il adauga in histograma fragmentului 
			respectiv, construind in acelasi timp o lista de cuvinte maximale.
			ii) Etapa de reducere
				- combinare
					Pentru etapa de combinare, se vor parcurge toate maximele 
				si hash-urile locale pentru a construi o lista de cuvinte maxime
				global si pentru a combina hash-urile(adunand efectiv valorile
				pentru fiecare cheie) in scopul formarii unei histograme a 
				intregului document.
				- procesare
					Etapa de procesare va aplica formula descrisa in enunt 
				pentru a calcula page rank-ul documentului curent. Ulterior,
				se vor elimina duplicatele din lista de cuvinte maximale pentru
				a determina numarul de cuvinte maximale distincte de fisier(
				lungimea lor fiind usor de determinat ca fiind lungimea 
				oricaruia dintre cuvinte.)	
				
	5. Scalabilitate si consistenta
		Testarea scalabilitatii a fost facuta pe un caz de test special, format
	din toate documentele referinta. Astfel, pentru un numar de 16 documente 
	algoritmul a fost rulat de mai multe ori pentru 1, 2, 4, sau 8 thread-uri
	si s-au comparat rezultatele. S-a remarcat o imbunatarie semnificativa 
	a timpului de executie impreuna cu cresterea numarului de thread-uri.
		In mod similar, prin rularea de 10 ori a testelor pentru un numar de 1,
	2, sau 4 thread-uri s-a observat ca rezultatele raman constante pentru 
	fiecare task.
	
	6. Probleme intampinate si solutii gasite
		Problema principala intampinata a fost sincronizarea thread-urilor 
	pentru a evita scrieri multiple a rezultatelor obtinute de fiecare worker in
	aceeasi lista de rezultate. 
		Solutia gasita a constat in folosrea unui lock (declarat ca fiind un 
	obiect generic static si final in clasa de utilitati), al carui lock 
	intrinsec a fost folosit pentru a asigura ca doar unul dintre workeri isi va 
	publica rezultatele la un anumit moment de timp.
	
	7. Compilare
		Fisierul build.xml contine informatiile necesare compilarii cu 
	utilitarul ant: ant compile jar.
		Compilarea va crea un fisier jar executabil mapreduce.jar, conform 
	enuntului temei.
		Subliniez ca versiunea de Java folosita in realizarea temei este 1.7,
	fisierul build.xml va avea ca target compilatorul de JDK-ul de Java 1.7.